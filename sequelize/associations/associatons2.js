var Sequelize = require('sequelize');

var sequelize = new Sequelize('sqlite://localhost/associatons2',
    {
        "storage": "/tmp/associations2.sqlite",
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false
        }
    }
);

var User = sequelize.define('User', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        },
        name: {
            type: Sequelize.STRING(255),
            comment: 'Full name of the user.'
        }
    }
);

var Group = sequelize.define('Group', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        }
    }
);

var Permission = sequelize.define('Permission', {
        targetId: {
            type: Sequelize.UUID,
            allowNull: false,
            comment: 'Target - The target to which permissions are given. Any Objects ID.'
        },
        toId: {
            type: Sequelize.UUID,
            allowNull: false,
            comment: 'To - To whom the permissions are given. Any Objects ID.'
        },
        level: {
            type: Sequelize.ENUM,
            values: [1, 2, 3],
            allowNull: false,
            comment: '1 - read, 2 - edit, 3 - admin'
        }
    }
);

// Group can have many members. A member is anyone who has at least read Permission.
Group.hasMany(User, {through: Permission, foreignKey: 'targetId', as: 'member', constraints: false}); // constraints: false  - as the "targetId" can be any Object id
User.hasMany(Group, {through: Permission, foreignKey: 'toId', as: 'membership', constraints: false}); // constraints: false  - as the "toId" can be any Object id

sequelize.sync().success(function () {
    console.log('---------------- Sync success ----------------');

    var user1 = User.build({name: 'User1'});

    var group = Group.build();

    user1.save().success(function () {
        var userPermissions = Permission.build({
            targetId: group.id,
            toId: user1.id,
            level: 3
        });

        group.addMember(userPermissions);

        group.save().success(function () {
            sequelize.query('SELECT * FROM Permissions'
                , null
                , {raw: true}
            ).success(function (results) {
                    console.log(JSON.stringify(results, null, 4));
                }
            );
        });
    });
});