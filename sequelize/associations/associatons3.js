var Sequelize = require('sequelize');

var sequelize = new Sequelize('sqlite://localhost/associatons3',
    {
        "storage": "/tmp/associations3.sqlite",
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "syncOnAssociation": true,
            "underscored": false
        }
    }
);

var Topic = sequelize.define('Topic', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        },
        title: {
            type: Sequelize.STRING(50),
            comment: 'Title of the Topic.',
            allowNull: false
        }
    }
);

var Group = sequelize.define('Group', {
    id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.UUIDV4
    },
    name: {
        type: Sequelize.STRING(255),
        comment: 'Name of the Group.',
        allowNull: false
    }
});


var TopicMemberGroup = sequelize.define('TopicMemberGroup', {
    groupId: {
        type: Sequelize.UUID,
        allowNull: false
    },
    topicId: {
        type: Sequelize.UUID,
        allowNull: false
    },
    level: {
        type: Sequelize.ENUM,
        values: ['admin', 'edit', 'read', 'none'],
        allowNull: false
    }
});

Topic.hasMany(Group, {
    through: TopicMemberGroup,
    foreignKey: 'topicId',
    as: {singular: 'group', plural: 'groups'},
    constraints: true
});

Group.hasMany(Topic, {
    through: TopicMemberGroup,
    foreignKey: 'groupId',
    constraints: true
});

sequelize.sync().success(function () {
    console.log('---------------- Sync success ----------------');

    var topic = Topic.build({
        title: 'Topic title'
    });

    var group = Group.build({
        name: 'Group name'
    });

    topic
        .save()
        .then(function(){
            return group.save()
        })
        .then(function(){
            return topic.addGroup(group, {level: 'admin'});
        })
        .then(function(){
            return Topic
                .find({
                    where: {id: topic.id},
                    include: [{model: Group, as: 'groups'}] // IF "as" is defined in the association, here the "as" has to exist as well.
                });
        })
        .success(function(res){
            console.log('success', res.toJSON());
        })
        .error(function(err){
            console.error('err', err);
        })
        .catch(function(err){
            console.error('catch', err);
        });
});