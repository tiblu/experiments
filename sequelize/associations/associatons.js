var Sequelize = require('sequelize');

var sequelize = new Sequelize('sqlite://localhost/associatons',
    {
        "storage": "/tmp/associations.sqlite",
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false
        }
    }
);

var User = sequelize.define('User', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        },
        name: {
            type: Sequelize.STRING(255),
            comment: 'Full name of the user.'
        }
    }
);

var Group = sequelize.define('Group', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        }
    }
);

// Every Group is greated by a User whom we call "the Creator"
Group.belongsTo(User, {as: 'creator'});

// Every Group has several Members (Users who belong to the Group)
Group.hasMany(User, {as: 'members', through: 'GroupMembers', constraints: false});
User.hasMany(Group, {as: 'members', through: 'GroupMembers', constraints: false});

sequelize.sync().success(function () {
    console.log('---------------- Sync success ----------------');

    var user1 = User.build({name: 'User1'});
    var user2 = User.build({name: 'User2'});

    var group = Group.build({creatorId: user1.id});

    user1.save().success(function () {
        user2.save().success(function () {
            group.addMembers([user1, user2]);

            group.save().success(function () {
                Group.findAndCountAll({
                    where: {
                        id: group.id
                    },
                    attributes: [
                        'id'
                    ],
                    include: [
                        { model: User, as: 'members', attributes: ['id'] }
                    ],
                    group: ['members.id']
                }).success(function (results) {
                    console.log(JSON.stringify(results, null, 4));
                });
//
//                sequelize.query('SELECT G.*, count(GM.UserId) as memberCount FROM Groups G LEFT JOIN GroupMembers GM ON (G.id = GM.GroupId) WHERE GM.UserId = :userId'
//                    , null
//                    , {raw: true}
//                    , {userId: user1.id}
//                ).success(function (results) {
//                        console.log(results);
//                        console.log(JSON.stringify(results, null, 4));
//                    }
//                );
            });
        });
    });
});