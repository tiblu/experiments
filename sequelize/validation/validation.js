var Sequelize = require('sequelize');

var sequelize = new Sequelize('sqlite://localhost/validation',
    {
        "storage": "/tmp/validation.sqlite",
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false
        }
    }
);

var User = sequelize.define('User', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        },
        name: {
            type: Sequelize.STRING(255),
            comment: 'Full name of the user.',
            validate: {
                isName: function(v){
                    console.log('NAME: validator run!');
                }
            }
        },
        password: {
            type: Sequelize.STRING(64),
            validate: {
                isPassword: function(v){
                    console.log('PASSWORD: validator run!');
                }
            }
        }
    }
);

sequelize.sync().success(function () {
    console.log('---------------- Sync success ----------------');

    User
        .create({
            name: 'User1',
            password: null
        })
        .success(function(){
            console.log('User1 created successfully!');
        })
        .error(function(err){
            console.log('User1 creation failed', err);
        });
});