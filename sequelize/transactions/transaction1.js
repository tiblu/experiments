'use strict';

/**
 * Created by m on 20.09.17.
 */

var Sequelize = require('sequelize');
var Promise = require('bluebird');

var sequelize = new Sequelize('postgres://experiments:experiments@localhost:5433/experiments',
    {
        "dialect": 'postgres',
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false,
            "syncOnAssociation": false,
            "freezeTableName": false,
            "charset": "utf8",
            "collate": "utf8_general_ci",
            "timestamps": true,
            "paranoid": true
        }
    }
);

sequelize
    .authenticate()
    .then(function () {
        console.log('Connection has been established successfully.');
    })
    .catch(function (err) {
        console.error('Unable to connect to the database:', err);
    });

const User = sequelize.define('user', {
    firstName: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING
    }
});

// force: true will drop the table if it already exists
User
    .sync({force: true})
    .then(function () {
        return sequelize.transaction(function (t) {
            var promisesToResolve = [];

            var userPromise1 = User
                .create({
                        firstName: 'John1',
                        lastName: 'Hancock1'
                    },
                    {transaction: t}
                );

            var userPromise2 = User.create
                ({
                        firstName: 'John2',
                        lastName: 'Hancock2'
                    },
                    {transaction: t}
                )
                .then(function () {
                    return Promise.reject('User2 reject');
                });

            promisesToResolve.push(userPromise1);
            promisesToResolve.push(userPromise2);

            return Promise
                .all(promisesToResolve)
                .then(function(){
                    return 'yes!';
                })
                .catch(function(){
                    console.log('asd');
                });
        });
    });