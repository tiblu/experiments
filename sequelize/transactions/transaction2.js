'use strict';

/**
 * Created by i on 08.11.17.
 */

var Sequelize = require('sequelize');
var Promise = require('bluebird');

var sequelize = new Sequelize('postgres://app:app@localhost:5433/experiments',
    {
        "dialect": 'postgres',
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false,
            "syncOnAssociation": false,
            "freezeTableName": false,
            "charset": "utf8",
            "collate": "utf8_general_ci",
            "timestamps": true,
            "paranoid": true
        }
    }
);

sequelize
    .authenticate()
    .then(function () {
        console.log('Connection has been established successfully.');
    })
    .catch(function (err) {
        console.error('Unable to connect to the database:', err);
    });

const User = sequelize.define('user', {
    firstName: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING
    }
});

const Book = sequelize.define('book', {
    title: {
        type: Sequelize.STRING
    },
    userName: {
        type: Sequelize.STRING
    }
});
// force: true will drop the table if it already exists
User.beforeCreate(function (instance, options) {
    console.log('INSTANCE', instance);
        console.log('OPTIONS', options);
    if(!options.transaction) {
        return sequelize.transaction(function (t) {
            return User
                .create(instance.dataValues, {transaction:t})
        }).then(function () {
            return Promise.reject();
        })
    }
});

User.afterCreate(function (instance, options) {
        return Book.create({
                title: 'My book'+instance.id,
                userName: instance.id
            }, {
                transaction:options.transaction
            })
            .then(function () {
                if(instance.firstName === 'John2') {
                    return Promise.reject('User2 reject');
                }
            })
});
sequelize
    .sync({force: true})
    .then(function () {
            var promisesToResolve = [];

            var userPromise1 = User
                .create({
                        firstName: 'John1',
                        lastName: 'Hancock1'
                    }
                )
                .catch(function(err) {
                    console.log(err);
                })

            var userPromise2 = User.create
                ({
                        firstName: 'John2',
                        lastName: 'Hancock2'
                    }
                )
                .then(function () {
                    return Promise.reject('User2 reject');
                })
                .catch(function(err) {
                    console.log(err);
                })

            promisesToResolve.push(userPromise1);
            promisesToResolve.push(userPromise2);

            return Promise
                .all(promisesToResolve)
                .then(function(){
                    return 'yes!';
                })
                .catch(function(){
                    console.log('asd');
                });
    });