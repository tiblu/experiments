'use strict';

var Sequelize = require('sequelize');
var lodash = require('lodash');


var sequelize = new Sequelize('postgres://experiments:experiments@localhost:5433/experiments',
    {
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false,
            "freezeTableName": false,
            "syncOnAssociation": false,
            "charset": "utf8",
            "collate": "utf8_general_ci",
            "timestamps": true,
            "paranoid": true
        },
        "omitNull": true
    }
);

sequelize.sync().then(function () {
    console.log('---------------- Sync success ----------------');
    var expectedToBeUUID = sequelize.Utils.toDefaultValue(Sequelize.UUIDV4);
    console.log('sequelize.Utils.toDefaultValue(Sequelize.UUIDV4) = ', expectedToBeUUID, typeof expectedToBeUUID);
    console.log('Sequelize.UUIDV4 lodash.isFunction = ', typeof sequelize.Utils.toDefaultValue(Sequelize.UUIDV4));
    console.log('sequelize.Utils.toDefaultValue is = ', sequelize.Utils.toDefaultValue); // Lets see what function body is...
    console.log('Sequelize.UUIDV4 is = ', Sequelize.UUIDV4); // Lets see what Sequelize.UUIDV4 is ...
    console.log('Sequelize.UUIDV4 lodash.isFunction = ', lodash.isFunction(Sequelize.UUIDV4));
    console.log('sequelize.Utils.toDefaultValue(Sequelize.UUIDV4()) = ', sequelize.Utils.toDefaultValue(Sequelize.UUIDV4()));
});