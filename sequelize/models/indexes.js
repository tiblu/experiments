'use strict';

var Sequelize = require('sequelize');

var sequelize = new Sequelize('postgres://experiments:experiments@localhost:5433/experiments',
    {
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false,
            "freezeTableName": false,
            "syncOnAssociation": false,
            "charset": "utf8",
            "collate": "utf8_general_ci",
            "timestamps": true,
            "paranoid": true
        },
        "omitNull": true
    }
);

var Topic = sequelize.define('Topic', {
        title: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    {
        indexes: [
            {
                fields: ['deletedAt'],
                where: {
                    deletedAt: {
                        $eq: null
                    }
                }
            }
        ]
    }
);

sequelize.sync({
    force: true,
    logging: console.log
}).then(function () {
    console.log('---------------- Sync success ----------------');

    Topic
        .create({
            title: 'foo'
        })
        .then(function () {
            console.log('created index test!')
        })
        .catch(function (err) {
            console.log('ER!', err);
        });

});