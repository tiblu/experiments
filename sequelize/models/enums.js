'use strict';

var Sequelize = require('sequelize');

var sequelize = new Sequelize('postgres://experiments:experiments@localhost:5433/experiments',
    {
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false,
            "freezeTableName": false,
            "syncOnAssociation": false,
            "charset": "utf8",
            "collate": "utf8_general_ci",
            "timestamps": true,
            "paranoid": true
        },
        "omitNull": true
    }
);

var LEVELS = ['admin', 'read'];
var L = {
    admin: 'admin'
};
var Permission = sequelize.define('Permission', {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        levels: {
            type: Sequelize.ENUM,
            values: LEVELS,
            allowNull: false,
            defaultValue: L.admin, //read
            comment: 'Level of access.'
        }
    }
);

sequelize.sync().success(function () {
    console.log('---------------- Sync success ----------------');

    var permissionList = [
        {name: '1'},
        {name: '2'},
        {name: '3'}
    ];

    Permission
        .bulkCreate(permissionList, {
            fields: ['name', 'levels']
        })
        .success(function () {
            console.log('created permissions!')
        })
        .catch(function (err) {
            console.log('ER!', err);
        });

});