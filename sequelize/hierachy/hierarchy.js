'use strict';

/**
 * How to make hierarchical queries.
 *
 * http://www.sqlite.org/lang_with.html
 * http://en.wikipedia.org/wiki/Hierarchical_and_recursive_queries_in_SQL
 */

var Sequelize = require('sequelize');

var sequelize = new Sequelize('sqlite://localhost/hierarchy',
    {
        "storage": "/tmp/hierarchy.sqlite",
        "logging": console.log,
        "sync": {
            "force": true,
            "logging": console.log
        },
        "define": {
            "underscored": false
        }
    }
);

var Group = sequelize.define('Group', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        },
        name: {
            type: Sequelize.TEXT
        },
        parentId: {
            type: Sequelize.UUID,
            allowNull: true,
            defaultValue: null
        }
    }
);


sequelize.sync().success(function () {
    console.log('---------------- Sync success ----------------');

    var g1 = Group.build({name: '1'});
    var g2 = Group.build({name: '1.1', parentId: g1.id});
    var g3 = Group.build({name: '1.1.1', parentId: g2.id});


    g1.save().success(function () {
        g2.save().success(function () {
            g3.save().success(function () {
                console.log('--Groups were created--');

                // Get all Groups that are parents of g3
                sequelize
                    .query(
                    'WITH RECURSIVE groupTree AS(' +
                    '   SELECT id, parentId, name' +
                    '   FROM Groups WHERE id = :groupId' +
                    '   UNION ALL' +
                    '   SELECT g.id, g.parentId, g.name' +
                    '   FROM Groups g' +
                    '   INNER JOIN groupTree gt ON gt.parentId = g.id' +
                    ') ' +
                    'SELECT * FROM groupTree', null, {type: Sequelize.QueryTypes.SELECT, raw: true}, {groupId: g3.id})
                    .success(function (result) {
                        console.log(JSON.stringify(result, null, 4));
                    });
            });
        });
    });
});