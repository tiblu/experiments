'use strict';

var pg = require('pg');
var QueryStream = require('pg-query-stream');
var Sequelize = require('sequelize');

var uri = 'postgres://app:app@localhost:5433/app';

var db = new Sequelize(uri, {
    "logging": console.log,
    "benchmark": true,
    "sync": {
        "force": false,
        "logging": true
    },
    "define": {
        "underscored": false,
        "freezeTableName": false,
        "syncOnAssociation": false,
        "charset": "utf8",
        "collate": "utf8_general_ci",
        "timestamps": true,
        "paranoid": true
    },
    "omitNull": false,
    "dialectOptions": {
        "ssl": true
    },
    "pool": {
        "min": 0,
        "max": 100
    }
});
pg.defaults.parseInt8 = true;


var connectionManager = db.connectionManager;

connectionManager
    .getConnection()
    .then(function (connection) {
        var query = new QueryStream('SELECT * FROM generate_series(0, $1) num', [1000000]);

        var stream = connection.query(query);
        //release the client when the stream is finished
        stream.on('data', function (data) {
            console.log('data', data);
        });
        stream.on('error', function (err) {
            console.log('error', err);
            connectionManager.releaseConnection(connection);
        });
        stream.on('end', function () {
            console.log('end', arguments);
            connectionManager.releaseConnection(connection);
        });
    });