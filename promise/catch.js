var reject = Promise.reject('Default reject');
var resolve = Promise.resolve();

resolve
    .then(function () {
        console.log('Resolved');
        return Promise
            .reject('|inner param|')
            .catch(function(err){
                console.log('Rejected - inner', err)
            });
    })
    .then(function(){
        console.log('I may not expect this when starting with promises, but this is expected');
    })
    .catch(function (err) {
        console.log('Rejected - outer', err);
    });